#!/bin/sh

COMP="uncompressed zstd lz4 snappy gzip qat producer"

for c in $COMP
do
    echo "=== Start === Compression type: $c"
    make clean COMP=$c
    echo "===  End  === Compression type: $c"
    echo ""
done