#!/bin/sh

COMP="uncompressed zstd lz4 snappy gzip qat producer"

for c in $COMP
do
    echo "=== Start === Compression type: $c"
    COUNT=$(make offsets COMP=$c)
    SIZE=$(make size COMP=$c)
    let "AVG = SIZE / COUNT"
    RED=$(bc -l <<< "scale=2;($AVG/8192)")
    echo "$COUNT messages, size: $(numfmt --to=iec $SIZE), msg: $AVG (c/r: $RED)"
    echo "===  End  === Compression type: $c"
    echo ""
done
