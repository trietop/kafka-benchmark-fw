#!/bin/sh

COMP="uncompressed zstd lz4 snappy gzip qat producer"

for c in $COMP
do
    echo "=== Start === Compression type: $c"
    echo ">>> Producer [$c]"
    make producer COMP=$c
    echo "<<< Consumer [$c]"
    make consumer COMP=$c THREADS=20
    echo "===  End  === Compression type: $c"
    echo ""
done