#!/bin/sh

COMP="uncompressed zstd lz4 snappy gzip qat producer"

TH=$(nproc)
let "TH = TH - 10"

for c in $COMP
do
    #echo "=== Start === Compression type: $c"
    COUNT=$(make offsets COMP=$c)
    #echo "<<< Consumer [$c] - $COUNT records"
    make consumer COMP=$c THREADS=20 RECORDS=${COUNT} &
    #echo "===  End  === Compression type: $c"
    #echo ""
done

wait